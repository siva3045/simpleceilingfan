package com.main.ceilingfan;

import com.main.ceilingfan.impl.CeilingFanService;
import com.main.ceilingfan.modal.CeilingFan;

import java.util.Scanner;

public class CeilingFanDemo {
	 
	
     public static void main(String[] args) {
    	 CeilingFan fan = new CeilingFan(); // Fan Switch ON and at LOW speed
    	 CeilingFanService fanSrv = new CeilingFanService();
    	 
    	 System.out.println("FAN speed : " + fan.getSpeed() +", Direction : " + fan.getDirection());
		 int cord;
		 askUser();

		 while (true) {
			Scanner scanner = new Scanner(System.in);
			 try {
				 cord=scanner.nextInt();
			 } catch (Exception e) {
				 cord=0;
			 }

			 if (cord == 1){
				 fanSrv.pullCord(fan);
				 printResult(fan);
			 }else if (cord == 2){
				 fanSrv.reverseCord(fan);
				 printResult(fan);
			 }else{
				 System.out.println("please enter valid input");

			 }
			askUser();
		 }
	 }

	private static void askUser() {
		System.out.println("1) pull Speed Cord  2) pull Direction cord");
	}

	private static void printResult(CeilingFan fan) {
		System.out.println("FAN speed : " + fan.getSpeed() +", Direction : " + fan.getDirection());
	}

}
