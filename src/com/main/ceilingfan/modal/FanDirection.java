package com.main.ceilingfan.modal;

public enum FanDirection {
    CLOCKWISE,
    ANTI_CLOCKWISE
}
