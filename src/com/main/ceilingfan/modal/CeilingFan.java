package com.main.ceilingfan.modal;

public class CeilingFan {

	private FanSpeed speed;
	private boolean on;
	private FanDirection direction;

	/** Constructor that creates a default fan */
	public CeilingFan() {
		speed = FanSpeed.LOW;
		on = true;
		direction=FanDirection.CLOCKWISE;
	}

	public FanSpeed getSpeed() {
		return speed;
	}

	public void setSpeed(FanSpeed speed) {
		this.speed = speed;
	}

	public FanDirection getDirection() { return direction; }

	public void setDirection(FanDirection direction) { this.direction = direction; }

	public boolean isOn() {
		return on;
	}

	public void setOn(boolean on) {
		this.on = on;
	}
	
	
	
}
